import pandas as pd
import os
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt1

## merging 12 months of data in one file

def merge12months():
    df = pd.read_csv('../Sales_Data/Sales_April_2019.csv')

    files = [file for file in os.listdir('../Sales_Data')]
    all_months_data = pd.DataFrame()

    for file in files:
        df = pd.read_csv('../Sales_Data/' + file)
        all_months_data = pd.concat([all_months_data, df])

    all_months_data.to_csv('.all_data.csv', index=False)


def read_new_data():
    all_data = pd.read_csv('all_data.csv')

    ## Augment data with additional columns

    ## add a month column

    all_data['month'] = all_data['Order Date'].str[0:2]

    ## clean the NaN values
    ## drop rows of NaN

    nan_df = all_data[all_data.isna().any(axis=1)]
    all_data = all_data.dropna(how='all')

    ## find OR and delete it

    all_data = all_data[all_data['Order Date'].str[0:2] != 'Or']
    all_data['month'] = all_data['month'].astype('int32')
    # print(all_data.head())

    ## add a sales column
    all_data['Quantity Ordered'] = pd.to_numeric(all_data['Quantity Ordered'])
    all_data['Price Each'] = pd.to_numeric(all_data['Price Each'])
    all_data['Sales'] = all_data['Quantity Ordered'] * all_data['Price Each']

    ## what was the best month for sales? How much was earned that month?

    results = all_data.groupby('month').sum().astype('float32')
    months = range(1, 13)
    #plt.bar(months, results['Sales'])
    #plt.xticks(months)
    #plt.ylabel('Sales in USD $')
    #plt.xlabel('Month Number')
    #plt.show()

    ## What city had the highest number of sales
    ## add a city column using .apply

    def get_city(address):
        return address.split(',')[1]

    def get_state(address):
        return address.split(',')[2].split(' ')[1]

    all_data['City'] = all_data['Purchase Address'].apply(lambda x: get_city(x) + '(' + get_state(x) + ')')
    results1 = all_data.groupby('City').sum()
    cities = [city for city, df in all_data.groupby('City')]
    plt1.bar(cities, results1['Sales'])
    plt1.xticks(cities, rotation='vertical', size=8)
    plt1.ylabel('sales in USD $')
    plt1.xlabel('Cities')
    plt1.show()

    print(all_data)

    ### next
if __name__ == '__main__':
    read_new_data()
